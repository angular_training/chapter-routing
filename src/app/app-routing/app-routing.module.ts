import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { UsersComponent } from '../users/users.component';
import { UserComponent } from '../users/user/user.component';
import { TasksComponent } from '../tasks/tasks.component';
import { TaskComponent } from '../tasks/task/task.component';
import { EditTaskComponent } from '../tasks/edit-task/edit-task.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';

const appRoutes: Route[] = [
  { path: '', component: HomeComponent },
  { path: 'users', component: UsersComponent, children: [
    { path: ':id/:name', component: UserComponent }
  ] },
  { path: 'tasks', component: TasksComponent, children: [
    { path: ':id', component: TaskComponent },
    { path: ':id/edit', component: EditTaskComponent }
  ] },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
