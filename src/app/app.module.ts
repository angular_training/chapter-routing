import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
//import { Route, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { TasksComponent } from './tasks/tasks.component';
import { UserComponent } from './users/user/user.component';
import { TaskComponent } from './tasks/task/task.component';
import { EditTaskComponent } from './tasks/edit-task/edit-task.component';
import { TasksService } from './services/tasks.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing/app-routing.module';

// const appRoutes: Route[] = [
//   { path: '', component: HomeComponent },
//   { path: 'users', component: UsersComponent, children: [
//     { path: ':id/:name', component: UserComponent }
//   ] },
//   { path: 'tasks', component: TasksComponent, children: [
//     { path: ':id', component: TaskComponent },
//     { path: ':id/edit', component: EditTaskComponent }
//   ] },
//   { path: 'not-found', component: PageNotFoundComponent },
//   { path: '**', redirectTo: '/not-found' }
// ];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UsersComponent,
    TasksComponent,
    UserComponent,
    TaskComponent,
    EditTaskComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
    //RouterModule.forRoot(appRoutes)
  ],
  providers: [TasksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
