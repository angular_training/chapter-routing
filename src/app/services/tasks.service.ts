
export class TasksService {

  tasks = [
    {
      id: 1,
      title: 'Master Angular',
      status: 'open'
    },
    {
      id: 2,
      title: 'Learn NodeJs',
      status: 'in progress'
    },
    {
      id: 3,
      title: 'Getting started GraphQL',
      status: 'archived'
    }
  ];

  getTasks() {
    return this.tasks;
  }

  getTask(id: number) {
    const task = this.tasks.find(
      (s) => {
        return s.id === id;
      }
    );
    return task;
  }

  addTask(task: {id: number, title: string, status: string}) {
    this.tasks.push(task);
  }

  updateTask(id: number, taskInfo: {title: string, status: string}) {
    const task = this.tasks.find(
      (s) => {
        return s.id === id;
      }
    );
    if (task) {
      task.title = taskInfo.title;
      task.status = taskInfo.status;
    }
  }

  updateTaskStatus(id: number, newStatus: string) {
    this.tasks[id].status = newStatus;
  }
}