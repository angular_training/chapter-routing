import { Component, OnInit } from '@angular/core';
import { TasksService } from 'src/app/services/tasks.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {

  task: {id: number, title: string, status: string};
  taskTitle = '';
  taskStatus = '';
  allowEdit: boolean = false;

  constructor(private tasksService: TasksService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      (queryParams: Params) => {
        this.allowEdit = queryParams['allowEdit'] === '1' ? true : false;
      }
    );

    this.route.params.subscribe(
      (params: Params) => {
        this.task = this.tasksService.getTask(+params['id']);
        this.taskTitle = this.task.title;
        this.taskStatus = this.task.status;
      }
    );
  }

  onUpdateTask() {
    this.tasksService.updateTask(this.task.id, {title: this.taskTitle, status: this.taskStatus});
  }

}
