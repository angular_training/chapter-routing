import { Component, OnInit } from '@angular/core';
import { TasksService } from 'src/app/services/tasks.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  task: {id: number, title: string, status: string};

  constructor(private tasksService: TasksService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const taskId = +this.route.snapshot.params['id'];
    this.task = this.tasksService.getTask(taskId);
    this.route.params.subscribe(
      (params) => {
        this.task = this.tasksService.getTask(+params['id']);
      }
    );
  }

  onEdit() {
    //this.router.navigate(['/tasks', this.task.id, 'edit']);
    this.router.navigate(['edit'], { relativeTo: this.route, queryParamsHandling: 'merge' });
  }

}
