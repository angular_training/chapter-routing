import { Component, OnInit } from '@angular/core';
import { TasksService } from '../services/tasks.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  private tasks: {id: number, title: string, status: string}[] = [];

  constructor(private tasksService: TasksService, 
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.tasks = this.tasksService.getTasks();
  }

  reloadTasksPage() {
    //this.router.navigate(['tasks'], { relativeTo: this.route});
  }

}
