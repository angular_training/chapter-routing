import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users = [
    {
      id: 1,
      name: 'Mohamed'
    },
    {
      id: 2,
      name: 'Anas'
    },
    {
      id: 3,
      name: 'Youssef'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
